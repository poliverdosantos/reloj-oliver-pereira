function updateTime() {
    var dateInfo = new Date();
  
    /* tiempo*/
    var hr,
      _min = (dateInfo.getMinutes() < 10) ? "0" + dateInfo.getMinutes() : dateInfo.getMinutes(),
      sec = (dateInfo.getSeconds() < 10) ? "0" + dateInfo.getSeconds() : dateInfo.getSeconds(),
      ampm = (dateInfo.getHours() >= 12) ? "PM" : "AM";
  
    if (dateInfo.getHours() == 0) {
      hr = 12;
    } else if (dateInfo.getHours() > 12) {
      hr = dateInfo.getHours() - 12;
    } else {
      hr = dateInfo.getHours();
    }
  
    var currentTime = hr + ":" + _min + ":" + sec;
  
    // imprime la hora
    document.getElementsByClassName("hms")[0].innerHTML = currentTime;
    document.getElementsByClassName("ampm")[0].innerHTML = ampm;
  
    /* dia */
    var dow = [
        "Domingo",
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
        "Sábado"
      ],/*mes*/
      month = [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
      ],
      day = dateInfo.getDate();
  
    // imprimiendo la fecha
    var currentDate = dow[dateInfo.getDay()] + ", "+ day +" de " + month[dateInfo.getMonth()] + " " ;
  
    document.getElementsByClassName("date")[0].innerHTML = currentDate;
  };
  

  updateTime();
  setInterval(function() {
    updateTime()
  }, 1000);